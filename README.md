# leipzig-theme
A beamer theme that conforms to the corporate design of the University of Leipzig.


Originally created by Lukas Gienapp at https://github.com/lgienapp/leipzig-theme

## Documentation
* read `example.pdf` and its source `example.tex`


## Installation
* sorry, no install script
* download the repository (small downward arrow in the upper right corner of the file table above)
* quick&dirty: copy the files `beamerthemeleipzig.sty`, `logo.pdf` and `ul-wordmark.pdf` into your working directory



## Changes
* forked from Lukas
* font and font handling changed for `pdflatex` compatibility
* `ul-wordmark` added
* small changes in documentatio/example tex file

